## Getting started

Building the docker image:
```
docker build -t gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-md-tutorial:latest .
```

Starting the jupyter notebook:
```
docker run --rm -it -p 8888:8888 gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-md-tutorial:latest
```
Note:
- follow the link in the terminal to open the notebook in your browser
- remove the "--rm" cli argument to keep the container after stopping it

## Development

Starting the jupyter notebook:
```
docker run --rm -it -p 8888:8888 -v "${PWD}/tutorial":/home/jovyan/ gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-md-tutorial:latest
```


