FROM jupyter/scipy-notebook:python-3.7

# ================================================================================
# Linux applications and libraries
# ================================================================================

# USER root
#
# RUN apt-get update --yes \
#  && apt-get install --yes --no-install-recommends \
#     openmpi-bin \
#     libopenmpi-dev \
#     cmake \
#     libblas-dev \
#     liblapack-dev \
#     gfortran \
#  && apt-get clean && rm -rf /var/lib/apt/lists/*
#
# USER ${NB_UID}

# ================================================================================
# Python environment
# ================================================================================

# RUN mamba install --quiet --yes \
#     "nbgitpuller" \
#  && mamba clean --all -f -y \
#  && fix-permissions "${CONDA_DIR}" \
#  && fix-permissions "/home/${NB_USER}"

RUN pip install --prefer-binary --no-cache-dir \
    "mdanalysis==2.1.0" \
    "nglview==3.0.3" \
    "scikit-learn" \
    "pymatgen" \
    "ipywidgets>=7.6.0,<8" \
    "git+https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR.git@develop#egg=nomad-lab" \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# Switch back to jovyan to avoid accidental container runs as root
USER ${NB_UID}
WORKDIR "${HOME}"

COPY --chown=${NB_UID} tutorial/ .
