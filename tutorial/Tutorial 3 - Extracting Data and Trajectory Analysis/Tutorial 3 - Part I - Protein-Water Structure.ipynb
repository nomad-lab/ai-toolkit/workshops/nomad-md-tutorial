{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <center> Part I: Analysis of protein solvation structure </center>\n",
    "\n",
    "In this part of the tutorial we will examine an atomistic molecular dynamics simulation of a protein dissolved in water. The globular protein considered here falls into a class of PDZ-2 domains which are known to exhibit allosteric behavior, i.e., a change in function (often accompanied by a structural change) upon ligand binding [[Varolgüneş *et al.*]](https://www.biorxiv.org/content/10.1101/2021.02.26.433010v1.abstract). Molecular simulations of allosteric proteins both with and without a bound ligand are often used to try to understand the allosteric phenomenon. In particular, one may be interested in the role that the solvation structure of water plays in the resulting structure of the protein in both states. Here we will look at the unbound protein and perform a simple structural analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import all the necessary modules. If you want to skip the molecular visualization, comment out nglview"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python\n",
    "import numpy as np\n",
    "\n",
    "# NOMAD tools\n",
    "from nomad.atomutils import archive_to_universe\n",
    "from nomad.atomutils import BeadGroup\n",
    "from nomad.datamodel import EntryArchive\n",
    "from nomad.units import ureg\n",
    "\n",
    "# I/O\n",
    "import json\n",
    "\n",
    "# Visualization\n",
    "import matplotlib.pyplot as plt\n",
    "import nglview as nv\n",
    "\n",
    "# MDAnalysis\n",
    "import MDAnalysis.analysis.rdf as MDA_RDF\n",
    "from MDAnalysis.analysis.hydrogenbonds import HydrogenBondAnalysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, you can use the [NOMAD API](https://nomad-lab.eu/prod/rae/docs/api.html) to grab particular archive entries from the repository or to search the repository for entries with certain attributes. Here, the simulation data has been pre-parsed and the resulting NOMAD archive entry is provided in this directory in json format. First, read in the .json file: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# should take ~10 s\n",
    "with open('data/PDZ-2_protein_archive.json', 'r') as fp:\n",
    "    data = json.load(fp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting variable `data` is a dictionary. The keys of this dictionary directly correspond to the sections that we examined in the **DATA** tab on the entry page of NOMAD in Tutorial 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's convert this dictionary into the internal NOMAD archive entry format, which is slightly more convenient to work with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# should take ~15 s\n",
    "archive = EntryArchive.m_from_dict(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we define some sections that will be used in the following cells. Take a few minutes to search through the archive, you may want to reference Tutorial 1 to help you navigate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "section_run = archive.run[-1]\n",
    "section_system = section_run.system\n",
    "section_system_topology = section_run.system[0].atoms_group\n",
    "section_atoms = section_system[0].atoms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall that the section **run** &rarr; **system**  is a list containing configurational information from each frame in the trajectory. The following code snippet extracts the positions, velocities, and box vectors from this list: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_atoms = section_atoms.get('n_atoms')\n",
    "n_frames = len(section_system) if section_system is not None else None\n",
    "atom_names = section_atoms.get('labels')\n",
    "\n",
    "# get the atom positions, velocites, and box dimensions\n",
    "positions = np.empty(shape=(n_frames, n_atoms, 3))\n",
    "velocities = np.empty(shape=(n_frames, n_atoms, 3))\n",
    "dimensions = np.empty(shape=(n_frames, 6))\n",
    "for frame_ind, frame in enumerate(section_system):\n",
    "    sec_atoms_fr = frame.get('atoms')\n",
    "    if sec_atoms_fr is not None:\n",
    "        positions_frame = sec_atoms_fr.positions\n",
    "        positions[frame_ind] = ureg.convert(positions_frame.magnitude, positions_frame.units,\n",
    "                                            ureg.angstrom) if positions_frame is not None else None\n",
    "        velocities_frame = sec_atoms_fr.velocities\n",
    "        velocities[frame_ind] = ureg.convert(velocities_frame.magnitude, velocities_frame.units,\n",
    "                                                ureg.angstrom / ureg.picosecond) if velocities_frame is not None else None\n",
    "        latt_vec_tmp = sec_atoms_fr.get('lattice_vectors')\n",
    "        if latt_vec_tmp is not None:\n",
    "            length_conversion = ureg.convert(1.0, sec_atoms_fr.lattice_vectors.units, ureg.angstrom)\n",
    "            dimensions[frame_ind] = [\n",
    "                sec_atoms_fr.lattice_vectors.magnitude[0][0] * length_conversion,\n",
    "                sec_atoms_fr.lattice_vectors.magnitude[1][1] * length_conversion,\n",
    "                sec_atoms_fr.lattice_vectors.magnitude[2][2] * length_conversion,\n",
    "                90, 90, 90]  # nb -- for cubic box!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Exercise 1\n",
    "\n",
    "Fill in the missing variables assignments to make a plot of the temperature as a function of time for this simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10,4))\n",
    "section_calculation =  ## FIND THE SECTION CALCULATION IN THE ARCHIVE ##   \n",
    "temperature = []\n",
    "time = []\n",
    "temperature_unit =   ## FIND THE UNIT OF TEMPERATURE USED IN THE ARCHIVE ##    \n",
    "time_unit =   ## FIND THE UNIT OF TIME USED IN THE ARCHIVE ##   \n",
    "for calc in section_calculation:\n",
    "    temperature.append()  ## FIND THE TEMPERATURE FOR THIS CALC ##\n",
    "    time.append()  ## FIND THE TIME FOR THIS CALC ##\n",
    "\n",
    "plt.plot(time, temperature)\n",
    "plt.ylabel(temperature_unit, fontsize=12)\n",
    "plt.xlabel(time_unit, fontsize=12)\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "#### Solution 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10,4))\n",
    "section_calculation = archive.run[-1].calculation  ## FIND THE SECTION CALCULATION IN THE ARCHIVE ##   \n",
    "temperature = []\n",
    "time = []\n",
    "temperature_unit = section_calculation[0].temperature.units  ## FIND THE UNIT OF TEMPERATURE USED IN THE ARCHIVE ##    \n",
    "time_unit = section_calculation[0].time.units  ## FIND THE UNIT OF TIME USED IN THE ARCHIVE ##   \n",
    "for calc in section_calculation:\n",
    "    temperature.append(calc.temperature.magnitude)  ## FIND THE TEMPERATURE FOR THIS CALC ##\n",
    "    time.append(calc.time.magnitude)  ## FIND THE TIME FOR THIS CALC ##\n",
    "\n",
    "\n",
    "plt.plot(time, temperature)\n",
    "plt.ylabel(temperature_unit, fontsize=12)\n",
    "plt.xlabel(time_unit, fontsize=12)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Exercise 2\n",
    "\n",
    "Now let's plot the water-water center of mass rdf averaged over the last 80% of the trajectory, similar to what was displayed on the overview page in Tutorial 2: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(8,4))\n",
    "section_MD =   ## FIND THE MOLECULAR DYNAMICS WORKFLOW SECTION IN THE ARCHIVE ##\n",
    "rdf_SOL_SOL =    ## FIND THE LAST SOL-SOL RDF STORED IN THE ARCHIVE ##\n",
    "rdf_start =    ## FIND THE STARTING FRAME FOR AVERAGING FOR THIS RDF ##\n",
    "rdf_end =    ## FIND THE ENDING FRAME FOR AVERAGING FOR THIS RDF ##\n",
    "\n",
    "bins = ureg.convert(rdf_SOL_SOL.bins.magnitude, rdf_SOL_SOL.bins.units, ureg.angstrom)  \n",
    "\n",
    "plt.plot(bins, rdf_SOL_SOL.value)\n",
    "plt.xlabel(ureg.angstrom, fontsize=12)\n",
    "plt.ylabel('SOL-SOL rdf', fontsize=12)\n",
    "plt.xlim(0.1,10.0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "#### Solution 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(8,4))\n",
    "section_MD = archive.workflow[0].molecular_dynamics  ## FIND THE MOLECULAR DYNAMICS WORKFLOW SECTION IN THE ARCHIVE ##\n",
    "rdf_SOL_SOL = section_MD.results.radial_distribution_functions[0].radial_distribution_function_values[-1]  ## FIND THE LAST SOL-SOL RDF STORED IN THE ARCHIVE ##\n",
    "rdf_start = rdf_SOL_SOL.frame_start  ## FIND THE STARTING FRAME FOR AVERAGING FOR THIS RDF ##\n",
    "rdf_end = rdf_SOL_SOL.frame_end  ## FIND THE ENDING FRAME FOR AVERAGING FOR THIS RDF ##\n",
    "\n",
    "bins = ureg.convert(rdf_SOL_SOL.bins.magnitude, rdf_SOL_SOL.bins.units, ureg.angstrom)  \n",
    "\n",
    "plt.plot(bins, rdf_SOL_SOL.value)\n",
    "plt.xlabel(ureg.angstrom, fontsize=12)\n",
    "plt.ylabel('SOL-SOL rdf', fontsize=12)\n",
    "plt.xlim(0.1,10.0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "####"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is already clear that it would be useful to develop converters to store the archive for an MD simulation in a format more convenient to perform analysis. In particular, one may want to utilize existing analysis software to perform standard calculations. We have already implemented a converter to the *MDAnalysis* format:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# should take ~10 s\n",
    "universe = archive_to_universe(archive)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's use this format to examine the protein simulation in more detail. First, let's check which molecule type are present:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Molecule Types')\n",
    "print('--------------')\n",
    "for moltype in np.unique(universe.atoms.moltypes): \n",
    "    print(moltype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get acquainted with the *MDAnalysis* software by reproducing the water-water molecular rdf that we plotted above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get an atom group for the water\n",
    "AG_SOL = universe.select_atoms('moltype SOL')\n",
    "\n",
    "# Create a \"bead group\" for the water.\n",
    "# In MDAnalysis, it is not trivial to calculate center of mass rdfs. \n",
    "# The concept of bead groups comes from a known work-around. \n",
    "# This class is imported from the NOMAD software. \n",
    "BG_SOL = BeadGroup(AG_SOL, compound=\"fragments\")\n",
    "\n",
    "\n",
    "min_box_dimension = np.min(universe.trajectory[0].dimensions[:3])\n",
    "max_rdf_dist = min_box_dimension / 2\n",
    "n_bins = 200\n",
    "n_smooth = 2\n",
    "n_prune = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# should take ~2 min\n",
    "exclusion_block = (1, 1)  # for removing self-distance\n",
    "rdf = MDA_RDF.InterRDF(\n",
    "    BG_SOL, BG_SOL, range=(0, max_rdf_dist),\n",
    "    exclusion_block=exclusion_block, nbins=n_bins).run(\n",
    "    rdf_start, rdf_end, n_prune)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# smooth the rdf\n",
    "rdf.results.bins = rdf.results.bins[int(n_smooth / 2):-int(n_smooth / 2)]\n",
    "rdf.results.rdf = np.convolve(\n",
    "    rdf.results.rdf, np.ones((n_smooth,)) / n_smooth,\n",
    "    mode='same')[int(n_smooth / 2):-int(n_smooth / 2)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(8,4))\n",
    "plt.plot(bins, rdf_SOL_SOL.value, label='NOMAD archive', color='k', lw=2)\n",
    "plt.plot(rdf.results.bins, rdf.results.rdf, label='MDAnalysis', linestyle='--', color='r', lw=2)\n",
    "\n",
    "plt.legend(fontsize=16)\n",
    "plt.xlabel(ureg.angstrom, fontsize=12)\n",
    "plt.ylabel('SOL-SOL rdf', fontsize=12)\n",
    "plt.xlim(0.1, 10.)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's consider a more custom analysis for this system. It is known that the first LYS and the first PHE residues make hydrogen bonds to bind a histine ligand. Let's find the residue identifiers for these two, so that we can investigate the solvation structure around them in the unbound state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "protein = universe.select_atoms('protein')\n",
    "print('There are {} residues in the protein'.format(len(protein.residues)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "LYS_residues = universe.select_atoms('resname LYS')\n",
    "print('The following ids are LYS_residues: ' + ' '.join([str(i) for i in np.unique(LYS_residues.resids)]))\n",
    "LYS_binding_pocket_resid = LYS_residues.resids[0]\n",
    "print('Residue ' + str(LYS_binding_pocket_resid) + ' is associated with the binding pocket.')\n",
    "\n",
    "print('\\n')\n",
    "\n",
    "PHE_residues = universe.select_atoms('resname PHE')\n",
    "print('The following ids are PHE_residues: ' + ' '.join([str(i) for i in np.unique(PHE_residues.resids)]))\n",
    "PHE_binding_pocket_resid = PHE_residues.resids[0]\n",
    "print('Residue ' + str(PHE_binding_pocket_resid) + ' is associated with the binding pocket.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can visualize the system with NGLViewer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# select all atoms except for virtual sites associated with the water model\n",
    "# should take ~15 s\n",
    "AG_all = universe.select_atoms('not name DUMMY')\n",
    "for ts in universe.trajectory:  # make the molecules whole \n",
    "    AG_all.unwrap(compound='fragments')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up the viewer\n",
    "view = nv.show_mdanalysis(AG_all)\n",
    "view.center()\n",
    "view.clear()  # clear the initial representation automatically set up by nglview\n",
    "view.add_point('all')  # employ lightest rep\n",
    "# adjust the widget size\n",
    "view._set_size('700px', '600px')\n",
    "\n",
    "\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Highlight the protein"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "moltype = 'Protein'  \n",
    "view.clear()\n",
    "view.add_point('all')\n",
    "view.add_spacefill(moltype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remove the water molecules and highlight the binding pocket residues of interest"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "moltype = 'Protein' \n",
    "view.clear()\n",
    "view.add_ball_and_stick(moltype)\n",
    "# specify residue\n",
    "view.add_spacefill(f'(LYS and {LYS_binding_pocket_resid}) or (PHE and {PHE_binding_pocket_resid})')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use MDAnalysis to find the waters that are initially around the LYS residue in the initial configuration, and display them"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "local_water_LYS = universe.select_atoms(f' resname SOL and around 3.0 (resname LYS and resid {LYS_binding_pocket_resid})')\n",
    "local_water_PHE = universe.select_atoms(f' resname SOL and around 3.0 (resname PHE and resid {PHE_binding_pocket_resid})')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view.clear()\n",
    "view.add_licorice('Protein')\n",
    "view.add_spacefill(f'(LYS and {LYS_binding_pocket_resid})')\n",
    "water_selection_LYS = ' or '.join([str(i) for i in local_water_LYS.residues.resids])\n",
    "view.add_ball_and_stick(water_selection_LYS)\n",
    "view.add_spacefill(f'(PHE and {PHE_binding_pocket_resid})')\n",
    "water_selection_PHE = ' or '.join([str(i) for i in local_water_PHE.residues.resids])\n",
    "view.add_ball_and_stick(water_selection_PHE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perform hydrogen-bonding analysis on the LYS and PHE binding pocket residues "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hbonds_LYS = HydrogenBondAnalysis(\n",
    "    universe=universe,\n",
    "    donors_sel=None,\n",
    "    hydrogens_sel=\"resname SOL and name H\",\n",
    "    acceptors_sel=f'(resname LYS) and (resid {LYS_binding_pocket_resid})',\n",
    "    d_a_cutoff=3.0,\n",
    "    d_h_a_angle_cutoff=150,\n",
    "    update_selections=False\n",
    ")\n",
    "\n",
    "hbonds_PHE = HydrogenBondAnalysis(\n",
    "    universe=universe,\n",
    "    donors_sel=None,\n",
    "    hydrogens_sel=\"resname SOL and name H\",\n",
    "    acceptors_sel=f'(resname PHE) and (resid {PHE_binding_pocket_resid}) and (name O)',\n",
    "    d_a_cutoff=3.0,\n",
    "    d_h_a_angle_cutoff=150,\n",
    "    update_selections=False\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hbonds_LYS.run()\n",
    "hbonds_PHE.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10,4))\n",
    "plt.plot(hbonds_LYS.times, hbonds_LYS.count_by_time(), lw=2, label='LYS')\n",
    "plt.plot(hbonds_PHE.times, hbonds_PHE.count_by_time(), lw=2, label='PHE')\n",
    "\n",
    "plt.xlabel(\"Time (ps)\", fontsize=12)\n",
    "plt.ylabel(r\"$N_{HB}$\", fontsize=12)\n",
    "plt.yticks([0, 1, 2])\n",
    "\n",
    "plt.legend(fontsize=16)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  },
  "vscode": {
   "interpreter": {
    "hash": "081eae7306d4e5cfe2c2effcf6cd31095924eaefff61f42eadf9b87663df02cb"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
