{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <center> Part II: Analysis of polymer glass transition </center>\n",
    "\n",
    "In this part of the tutorial we will examine a series of atomistic molecular dynamics simulations of a polymer melt at different temperatures. We will follow the recent work of [Banerjee *et al.*](https://arxiv.org/abs/2211.14220) to identify the glass transition temperature from structural fluctuations of the polymer\n",
    "chains."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import all the necessary modules. If you want to skip the molecular visualization, comment out nglview"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python\n",
    "import numpy as np\n",
    "\n",
    "# NOMAD tools\n",
    "from nomad.atomutils import archive_to_universe\n",
    "from nomad.datamodel import EntryArchive\n",
    "from nomad.units import ureg\n",
    "\n",
    "# I/O\n",
    "import json\n",
    "import orjson\n",
    "\n",
    "# Visualization\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import cm\n",
    "from matplotlib import colorbar\n",
    "import nglview as nv\n",
    "\n",
    "# MDAnalysis\n",
    "from MDAnalysis.analysis.distances import self_distance_array, distance_array\n",
    "\n",
    "# Analysis\n",
    "from sklearn.decomposition import PCA\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "# from sklearn.preprocessing import MinMaxScaler\n",
    "# import seaborn as sns\n",
    "# import matplotlib as mpl\n",
    "\n",
    "from sklearn.cluster import AgglomerativeClustering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read in the provided archive entries, convert into the internal NOMAD archive entry format, and convert into the *MDAnalysis* format:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this could take 5-15 min\n",
    "path = './data/'\n",
    "temperatures = ['225', '275', '325', '375', '425', '475', '525', '575']\n",
    "archives = {}\n",
    "universes = {}\n",
    "for temp in temperatures:\n",
    "    print(temp)\n",
    "    with open(path+f'nba_15mer100_{temp}_eq2_archive.json', 'r') as fp:\n",
    "        data = orjson.loads(fp.read())\n",
    "        print(\"load\")\n",
    "        archives[temp] = EntryArchive.m_from_dict(data)\n",
    "        print(\"archive\")\n",
    "        universes[temp] = archive_to_universe(archives[temp])\n",
    "        print('done with temp = '+temp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create selection groups for analysis. Here we will use the positions of particular carbon atoms along the polymer chain (C5, C7, and C9 carbon types) as our input features. \n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/side_chain.png\" width=\"300px\" />\n",
    "</center>\n",
    "\n",
    "Additionally, for simplicity, we will only consider a single chain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_type_selections = ['C2', 'C3', 'C5', 'C7', 'C9']\n",
    "selection = 'type ' + ' or type '.join([type for type in atom_type_selections])\n",
    "atom_groups = {}\n",
    "for temp in temperatures:\n",
    "    atom_groups[temp] = universes[temp].select_atoms(f'molnum 0 and ({selection})')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's briefly visualize the system and our selection group using NGLViewer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# select all atoms from the 575 K simulation\n",
    "# this should take < 1 min\n",
    "AG_all = universes['575'].select_atoms('all')\n",
    "for ts in universes['575'].trajectory:  # make the molecules whole \n",
    "    AG_all.unwrap(compound='fragments')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up the viewer\n",
    "view = nv.show_mdanalysis(AG_all)\n",
    "view.center()\n",
    "view.clear()  # clear the initial representation automatically set up by nglview\n",
    "view.add_point('all')  # employ lightest rep\n",
    "# adjust the widget size\n",
    "view._set_size('700px', '600px')\n",
    "\n",
    "\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now select the first chain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view.clear()\n",
    "view.add_ball_and_stick('1-15')  # the first chain consists of residues 1-15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recreate our selection in the viewer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ngl_selection = '@'+','.join([str(i) for i in atom_groups['575'].indices] )  # select based on atom index\n",
    "\n",
    "view.clear()\n",
    "view.add_ball_and_stick('1-15')\n",
    "view.add_ball_and_stick(ngl_selection, aspectRatio=3, color='blue')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculate all pairwise distances between the selected carbons for each temperature and for each frame in the corresponding trajectory: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "distances = {}\n",
    "for temp in temperatures:\n",
    "    distances[temp] = []\n",
    "    for frame in universes[temp].trajectory:\n",
    "        if distances[temp] == []:\n",
    "            distances[temp] = self_distance_array(atom_groups[temp].positions, box=frame._unitcell)\n",
    "        else:\n",
    "            distances[temp] = np.vstack((distances[temp], self_distance_array(atom_groups[temp].positions, box=frame._unitcell)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`distances` is a dictionary with keys corresponding to the simulation temperatures. `distances[T]` is then an array with shape `(n_frames, n_pairs)` for each temperature `T`. Now, we will stack all the distances together to analyze all temperatures simultaneously:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "distances_all = []\n",
    "temp_labels = []\n",
    "for temp in temperatures:\n",
    "    if distances_all == []:\n",
    "        distances_all = distances[temp]\n",
    "        temp_labels = [temp] * distances[temp].shape[0]\n",
    "    else:\n",
    "        distances_all = np.vstack((distances_all, distances[temp]))\n",
    "        temp_labels = np.hstack((temp_labels, [temp] * distances[temp].shape[0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perform dimensionality reduction using principal component analysis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler = StandardScaler()\n",
    "scaler.fit(distances_all)\n",
    "descriptor = scaler.transform(distances_all)  # transform the distances to have zero mean\n",
    "variance_input = scaler.var_\n",
    "\n",
    "pca = PCA().fit(descriptor)\n",
    "pca_projection = PCA(n_components=None).fit_transform(descriptor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a scatter plot of the polymer configurations along the first two components of the PCA space, and color the points according to the corresponding temperature of the simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(6,4))\n",
    "color_wheel = iter(cm.rainbow_r(np.linspace(0, 1, len(temperatures))))\n",
    "for temp in temperatures[::-1]:\n",
    "    color = next(color_wheel)\n",
    "    temp_indices = np.where(temp_labels == temp )\n",
    "    ax = plt.scatter(pca_projection[temp_indices, 0], pca_projection[temp_indices, 1], color=color,  alpha = 0.5, label = temp)\n",
    "plt.legend()\n",
    "plt.xlabel('PC 1', fontsize=12)\n",
    "plt.ylabel('PC 2', fontsize=12)\n",
    "plt.legend(ncol = 2)\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now perform a clustering in PCA space to try to identify configurations below the glass transition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = StandardScaler().fit_transform(pca_projection[:,0:4])  # cluster in the first 4 PCA dimensions\n",
    "clustering = AgglomerativeClustering().fit(X)\n",
    "labels = clustering.labels_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cluster_labels = ['non-glassy', 'glassy']\n",
    "label_types = np.unique(labels)\n",
    "color_wheel = iter(cm.tab20_r(np.linspace(0, 1, len(label_types))))\n",
    "fig = plt.figure(figsize=(6,4))\n",
    "for label in label_types:\n",
    "    color = next(color_wheel)\n",
    "    label_indices = np.where(labels == label)[0]\n",
    "    plt.scatter(pca_projection[label_indices, 0], pca_projection[label_indices, 1], color=color, label=cluster_labels[label])\n",
    "plt.legend(fontsize=14)\n",
    "plt.xlabel('PC 1', fontsize=12)\n",
    "plt.ylabel('PC 2', fontsize=12)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  },
  "vscode": {
   "interpreter": {
    "hash": "7b8e120d5d17ce1f76c2699e4e3ff72559d2328edb97f8f99ea20af7b83deaed"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
