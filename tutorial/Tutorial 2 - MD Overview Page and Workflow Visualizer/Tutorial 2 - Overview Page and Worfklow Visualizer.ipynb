{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "33b52f7d-38c3-450b-958c-120f3b2015e0",
   "metadata": {},
   "source": [
    "# Tutorial 2: Molecular dynamics overview page and workflow visualizer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cdc71d9-bdcb-46eb-83b4-270d09360431",
   "metadata": {},
   "source": [
    "In this tutorial, we will examine the GUI features for molecular dynamics simulations in NOMAD, using as an example a simulation workflow of setting up and equilibrating a binary liquid mixture. For convenience, the simulation data has been pre-parsed and is provided in the NOMAD *Metainfo* format.\n",
    "\n",
    "In the current directory (`Tutorial 2 - MD Overview Page and Workflow Visualizer/`), you will find the file `binary_mixture_pre-parsed.zip`. Upload the file to [NOMAD](https://nomad-lab.eu/), as demonstrated in Tutorial 1. In the upload page, you should see 6 identified main files: &nbsp; `workflow_archive.json`, &nbsp; `minimEQ1_archive.json`, &nbsp; `EQ1_archive.json`, &nbsp; `EQ2_archive.json`, &nbsp; `EQ3_archive.json`, and &nbsp; `PD1_archive.json`. &nbsp; `workflow_archive.json` corresponds to a workflow entry that describes how the other main files are related to each other. Click on the 3 dots to the right of this workflow entry. You should see a workflow graph:\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/workflow_graph.png\" width=\"800px\" />\n",
    "</center>\n",
    "\n",
    "The inputs and outputs of the workflow are depicted with light blue circles. Clicking on the text label above these circles will take you to the *Metainfo* section within the **DATA** tab of the corresponding entry. The dark blue circles represent sub-workflows within this workflow. Here we have a linear workflow consisting of a geometry optimization (i.e., energy minimization), followed by 4 molecular dynamics simulations.\n",
    "\n",
    "Scroll down to the bottom of the page, where you will see references to each entry within the workflow:\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/workflow_references_pre-parsed.png\"  width=\"800px\"  /)\n",
    "</center>\n",
    "\n",
    "Click on the 3 dots next to the &nbsp; `minimEQ1_archive.json` entry. This will take you to the **OVERVIEW** page of the energy minimization step of the workflow.\n",
    "\n",
    "At the top of this page is the **Material** section, which provides an overview of the system, including a visualizer (activated by clicking *YES* on the prompt, as in Tutorial 1). To the left of the visualizer, there is an *accordion menu* displaying the molecular topology. By default, the entire system is selected. By selecting one of the molecule groups (**GROUP_MOL** or **GROUP_MON** in this case), the visualizer will display only molecules of the selected type and make all other molecules transparent (see image below). Beneath each molecule group is a representative molecule tab, which will display a single molecule of that type in the visualizer when clicked.\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/minimEQ1_material_card.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Now scroll down to the **Geometry optimization** section. This energy minimization run is automatically labeled as a *geometry optimization* workflow, and the energy convergence plot is displayed in the overview page if the relevant data is available.\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/minimEQ1_go_card.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Scroll down to the **Workflow Graph** section. Here an automatically generated workflow graph is displayed for this entry only. In this case, only a single configuration was saved from the energy minimization, such that the workflow consists of a single step. Notice the *Workflow parameters* input and the *Workflow results* output. Take a few minutes to examine these sections in the *Metainfo* by clicking their text labels.\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/minimEQ1_workflow.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "At the bottom of the page, there is an **Entry References** section, which links you to the overall workflow that we examined at the beginning of the tutorial (Note - you must open the toggle next to *Referenced by the following entries*). Click on the 3 dots next to this entry to return to the global workflow page for this upload.\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/minimEQ1_references.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Let's now examine the first molecular dynamics run of this workflow. Click on the *Molecular Dynamics* text label above the first dark blue circle with this label. This will take you to the corresponding overview page. The **Material** section is identical to the energy minimization entry, since all entries in this upload correspond to simulations of the same system.\n",
    "\n",
    "Scroll down to the **Thermodynamic properties** section. Here we find plots of the time trajectories of various thermodynamic quantities: temperature, pressure, and potential energy in this case. At the bottom of the section some basic information about the molecular dynamics run are displayed: the timestep and the thermodynamic ensemble. We can see immediately that this *NVT* simulation was run at ~1000 K and at a very low pressure (gas phase, in line with what we observed in the visualizer).\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/EQ1_thermodynamic_properties.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Scroll down to the **Structural properties** section. Once a molecular dynamics workflow is detected, the NOMAD software automatically tries to calculate radial distribution functions (rdfs) as a function of the molecular center of mass for each unique pair of molecule types. These rdfs are determined for various intervals of the trajectory, as a zeroth order measure of equilibration. As expected for the gas phase, the rdfs converge quite quickly here.\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/EQ1_structural_properties.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Scroll down to the **Dynamical properties** section. Similar to the rdfs, NOMAD also calculates the molecular mean squared displacements for each molecule type. (Note that this requires at least 50 time frames present in the trajectory). A simple linear fitting procedure over the entire resulting msd curve is performed to determine the diffusion constant (displayed in the legend along with the corresponding *Pearson correlation coefficient*).\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/EQ1_dynamic_properties.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Again, subsequent sections display the workflow graph for this entry and the reference to the global workflow for this upload. Note that a limited number of steps are displayed within the workflow graph. The solid arrow within the graph denotes that intermediate steps are not visualized.\n",
    "\n",
    "<center>\n",
    "  <img src=\"Images/EQ1_workflow.png\" width=\"800px\" /)\n",
    "</center>\n",
    "\n",
    "Now go back to the global workflow overview page. Take some time to examine the overview pages for the remainder of the molecular dynamics runs within the overall workflow for this upload, and answer the corresponding questions below.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce5372fc-6c32-4287-806c-156fc0d90edc",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Exercises\n",
    "\n",
    "Provide a description of the molecular dynamics run (for example, which ensemble? what temperature? is the system equilibrated? what phase is the system in?) for:\n",
    "\n",
    "1. the second molecular dynamics run within this workflow\n",
    "\n",
    "2. the third molecular dynamics run within this workflow\n",
    "\n",
    "3. the fourth molecular dynamics run within this workflow\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "529b6c2f-de91-4237-8927-a2151b43823e",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### Answers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1d21a65-2eec-4569-998e-1442425900ab",
   "metadata": {},
   "source": [
    "1. This is an NVT annealing simulation to cool the system from ~1000 k to ~300 k. As the temperature cools, the molecules aggregate, as indicated by the growing peaks in the rdfs. However, the pressure remains low due to the (fixed) large box size.\n",
    "\n",
    "2. This is an NPT simulation, performed at ~300 k. Mid-way through the simulation there is a clear transition in the potential energy and an increase in the pressure, indicating that the box is contracting and the system is entering a proper liquid phase. This is validated through the rdfs, which become properly normalized to 1 at large distances later in the trajectory.\n",
    "\n",
    "3. This is a fully equilibrated NPT simulation.\n",
    "\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
